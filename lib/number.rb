module Ponzi
  class Number
    attr_reader :external_rep

    def initialize(unparsed_number)
      @external_rep = unparsed_number
      # For the meantime.
      @value = unparsed_number.to_i
    end

    def eval
      @value
    end
  end
end
