module Ponzi
  class Symbol
    def initialize(name, environment)
      @name = name
      @environment = environment
    end

    def eval(cdr)
      # primatives
      result = @environment.process(@name, cdr)
    end
  end
end
