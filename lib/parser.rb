require 'lib/pair'

# NOTA BENE: This is a prototype for something that will be written in C, so
# I am prioritizing easy translation over idiomatic Ruby.
#
# That being said I'm putting a lot of regex in the prototype that won't be in
# the final product to save time. Also, hecka slow.
module Ponzi
  module Parser
    def Parser.special_initial?(c)
      !!(c =~ /^[!\$%&\*\/:<=>\?\^_~]$/)
    end

    def Parser.letter?(c)
      !!(c =~ /^[a-zA-Z]$/)
    end

    def Parser.initial?(c)
      Parser.special_initial?(c) || Parser.letter?(c)
    end

    def Parser.digit?(c)
      !!(c =~ /^[0-9]$/)
    end

    def Parser.special_subsequent?(c)
      !!(c =~ /^[+-.@]$/)
    end

    def Parser.subsequent?(c)
      Parser.initial?(c) || Parser.digit?(c) || Parser.special_subsequent?(c)
    end

    def Parser.peculiar_identifier?(str)
      str =~ /^[+-]/ || str == '...'
    end

    def Parser.tokenize(str)
      chars = str.chars
      mother_array = []
      mother_environment = Environment.new
      i = 0

      # Using a while loop for more easy conventional lookahead step parsing
      while i < chars.length
        # Parsing non-particular identifier
        if Parser.initial?(chars[i])
          token = chars[i]
          loop do
            i += 1
            break unless Parser.subsequent?(chars[i])
            token += chars[i]
          end
          puts "Identifier complete: #{token}"
          # TODO: For now, I'll just make a symbol, later we'll make our
          # decisions in the appropriate way
        elsif Parser.peculiar_identifier?(chars[i..i+2].join)
          # What about '........'?
          token = ''
          if chars[i] == '.'
            token = '...'
            i += 3
          else 
            token = chars[i]
            i += 1
          end
          # bookmark
          mother_array.push(Symbol.new(token, mother_environment))
        elsif chars[i] == '#'
          token = chars[i]
          # Boolean
          if chars[i+1] == 't'
            token = '#t'
            i += 2
          elsif chars[i+1] == 'f'
            token = '#f'
            i += 2
          elsif chars[i+1] == '\\'
            token = chars[i+2]
            i += 3
          else
            puts 'ERROR OH NO!'
          end
          puts "Successful #-initial token: #{token}"
        elsif Parser.digit?(chars[i]) 
          # Numbers with no exactness or radix
          # TODO Decimals, exponent markers, etc. will all go here.
          token = ''
          loop do
            break unless Parser.digit?(chars[i])
            token += chars[i]
            i += 1
          end
          mother_array.push(Number.new(token))
        elsif chars[i] == '"'
          token = ''
          loop do
            i += 1
            if chars[i] == '"'
              i += 1
              break
            elsif chars[i] == '\\'
              i += 1 if chars[i+1] == '"' || chars[i+1] == '\\'
            end
            token += chars[i]
          end
          puts "String parsed: #{token}"
        end

        i += 1 if chars[i] == ' '# TODO: in the long run, this should never be my itself
      end
      Pair.new(mother_array, mother_environment).eval
    end
  end
end
