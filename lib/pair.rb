module Ponzi
  class Pair < Pobject
    attr_reader :car, :cdr

    def initialize(data, environment = nil)
      super
      @environment = environment
      # It is preobably easier if data is always an array.
      if (data.empty?)
        @car = nil
        @cdr = nil
      else
        @car = data.first
        # TODO: I will probably need to create a new environment for each
        # level...
        @cdr = Pair.new(data[1..-1], @environment)
      end
    end

    def nil?
      @car.nil? && @cdr.nil?
    end

    def eval
      @car ? @car.eval(@cdr) : self
    end
  end
end
