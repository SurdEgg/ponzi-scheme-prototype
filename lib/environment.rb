# frozen_string_literal: true

module Ponzi
  # The ponzi environment, one of which will be referenced by every Ponzi
  # object.
  class Environment
    def initialize; end

    # TODO: type check
    def add(operand_list)
      sum = 0
      until operand_list.nil?
        sum += operand_list.car.eval
        operand_list = operand_list.cdr
      end
      sum
    end

    def process(name, cdr)
      # Primatives
      if name == '+'
        add(cdr)
      else
        'ERROR: Symbol not found!'
      end
    end
  end
end
