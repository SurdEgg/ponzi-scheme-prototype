#! /usr/bin/env ruby
# frozen_string_literal: true

$LOAD_PATH << File.dirname(__FILE__)

require 'lib/environment'
require 'lib/pobject'
require 'lib/symbol'
require 'lib/number'
require 'lib/parser'

loop do
  print '> '
  input_string = gets.chomp
  break if input_string == 'exit'
  p Ponzi::Parser.tokenize(input_string)
end

puts 'Goodbye!' 

=begin
top_level = PEnvironment.new

loop do
  print 'ponzi> '
  input = gets
  next if input.strip.length.zero?

  puts Ponzi.parse(input, top_level).eval(nil, top_level).to_s
end
=end
