# frozen_string_literal: true

$LOAD_PATH << Dir.pwd
require 'lib/parser'

RSpec.describe 'R5RS.1' do
  before(:all) do
    top_level = PEnvironment.new
    @top = top_level
  end

  def rep(input_string)
    Ponzi.parse(input_string, @top).eval.representation
  end

=begin
  describe '1.2 expressions' do
    it 'should evaluate a boolean to itself' do
      expect(rep('#t')).to eq('#t')
    end

    it 'should evaluate a single number to itself' do
      expect(rep('23')).to eq('23')
    end

    it 'should add two numbers properly' do
      expect(rep('(+ 23 42)')).to eq('65')
    end

    it 'should add subexpressions' do
      expect(rep('(+ 14 (+ 23 42))')).to eq('79')
    end

    it 'should to different type of arithmetic on nested subexpressions' do
      expect(rep('(+ 14 (* 23 42))')).to eq('980')
    end
  end

  describe '1.5 Forms' do
    it 'should be able to define a number and return the defined value' do
      expect(rep('(define x 23)')).to eq('23')
    end

    it 'should be able to do arithmetic with a defined value' do
      expect(rep('(define x 23)')).to eq('23')
      expect(rep('(* x 2)')).to eq('46')
    end
  end
=end
end
