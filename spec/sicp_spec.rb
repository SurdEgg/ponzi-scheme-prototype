$LOAD_PATH << Dir.pwd
require 'lib/parser'

RSpec.describe 'SICP' do
  before(:all) do
    top_level = PEnvironment.new
    @top = top_level
  end

  def rep(inputString)
    Ponzi.parse(inputString, @top).eval.to_s
  end

  describe '1.1 The Elements Of Programming' do
    it 'should evaluate a single number to itself' do
      expect(rep('486')).to eq('486')
    end

    it 'should handle basic addition' do
      expect(rep('(+ 137 349)')).to eq('486')
    end

    it 'should handle basic subtraction' do
      expect(rep('(- 1000 334)')).to eq('666')
    end

    it 'should handle basic multiplication' do
      expect(rep('(* 5 99)')).to eq('495')
    end

    it 'should handle basic division' do
      expect(rep('(/ 10 5)')).to eq('2')
    end

    it 'should be able to add floats to integers' do
      expect(rep('(+ 2.7 10)')).to eq('12.7')
    end

    it 'should be able to sum up more than two numbers' do
      expect(rep('(+ 21 35 12 7)')).to eq('75')
    end    
    
    it 'should be able to multiply more than two numbers' do
      expect(rep('(* 25 4 12)')).to eq('1200')
    end    
    
    it 'should be able to do nested arithmetic' do
      expect(rep('(+ (* 3 5) (- 10 6))')).to eq('19')
    end    
    
    it 'should be able to do HEVILY-nested arithmetic' do
      expect(rep('(+ (* 3 (+ (* 2 4) (+ 3 5))) (+ (- 10 7) 6))')).to eq('57')
    end
  end

  describe '1.2 Naming and the Environment' do
    it 'should be able to define a variable' do
      rep('(define size 2)')
      expect(rep('(size)')).to eq('2')
    end

    it 'should be able to do arithmetic with a variable' do
      expect(rep('(* 5 size)')).to eq('10')
    end

    it 'should be able to use variables to find a circle\'s area' do
      rep('(define pi 3.14159)')
      rep('(define radius 10)')
      expect(rep('(* pi (* radius radius))')).to eq('314.159')
    end

    it 'should be able to use variables to define a circle\'s circumference' do
      rep('(define pi 3.14159)')
      rep('(define radius 10)')
      rep('(define circumference (* 2 pi radius))')
      expect(rep('circumference')).to eq('62.8318')
    end

    it 'should be able to do more deeply-nested math' do
      expect(rep('(* (+ 2 (* 4 6)) (+ 3 5 7))')).to eq('390')
    end
  end

  describe '1.4 Compound Procedures:' do
    it 'Should be able to define a procedure' do
      expect(rep('(define (square x) (* x x)')).to eq('square')
    end

    it 'User defined procedures should work with simple integers' do
      expect(rep('(square 21)')).to eq('441')
    end

    it 'User defined procedures should work with nested expressions as args' do
      expect(rep('(square (+ 2 5))')).to eq('49')
    end

    it 'User defined procudures should work with themselves as nexted expressions' do
      expect(rep('(square (square 3))')).to eq('81')
    end

    it 'New procedures can be build with other new procedures' do
      expect(rep('(define (sum-of-squares x y) (+ (square x) (square y)))')).to eq('sum-of-squares')
    end

    it 'And so on' do
      expect(rep('(define (f a) (sum-of-squares (+ a 1) (* a 2)))')).to eq('f')
    end

    it 'Tying the whole section together...' do
      expect(rep('(f 5)')).to eq('136')
    end
  end
end
