$LOAD_PATH << Dir.pwd
require 'lib/parser'

RSpec.describe 'R5RS.1' do
  before(:all) do
    top_level = PEnvironment.new
    @top = top_level
  end

  def rep(inputString)
    Ponzi.parse(inputString, @top).eval.representation
  end

=begin
  describe '6.1 equivalence predicates' do
    it '#t should eqv? #t' do
      expect(rep('(eqv? #t #t)')).to eq('#t')
    end    
    
    it '#f should eqv? #f' do
      expect(rep('(eqv? #f #f)')).to eq('#t')
    end

    it '#t should not eqv? #f' do
      expect(rep('(eqv? #t #f)')).to eq('#f')
    end

    it '#f should eqv? #t' do
      expect(rep('(eqv? #f #t)')).to eq('#f')
    end

    it 'should be able to create symbols' do
      expect(rep('(define foo \'bar)')).to eq('bar')
    end
    
    it 'should be able to convert from symbol to string' do
      expect(rep('(symbol->string \'foo)')).to eq('"foo"')
    end

    it 'should be able to test string equality' do
      expect(rep('(string=? "foo" "foo")')).to eq('#t')
    end    

    it 'should be able to test string inequality' do
      expect(rep('(string=? "foo" "bar")')).to eq('#f')
    end

    # The following two tests are exaclty as defined in the standard.
    it 'eqv? should detect equal symbols' do
      expect(rep('(string=? (symbol->string \'foo)
          (symbol->string \'foo)')).to eq('#t')
    end

    it 'eqv? should detect unequal symbols' do
      expect(rep('(string=? (symbol->string \'foo)
          (symbol->string \'bar)')).to eq('#f')
    end
  end
=end
end
