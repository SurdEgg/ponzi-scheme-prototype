#! /usr/bin/env ruby
# frozen_string_literal: true

$LOAD_PATH << File.dirname(__FILE__)
require 'lib/parser'

top_level = PEnvironment.new

loop do
  print 'ponzi> '
  input = gets
  next if input.strip.length.zero?

  puts Ponzi.parse(input, top_level).eval(nil, top_level).to_s
end
