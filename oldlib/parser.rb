require 'lib/ppair'
require 'lib/pvector'
require 'lib/pstring'
require 'lib/pnumber'
require 'lib/pboolean'
require 'lib/penvironment'
require 'lib/psymbol'
require 'lib/plibraryprocedure'
require 'lib/pproc'

module Ponzi
  def self.lex(atom, environment)
    if atom =~ /^[0-9\.]+$/
      PNumber.new(atom)
    elsif atom =~ /^#(t|f)$/
      PBoolean.new(atom)
    else
      environment_result = environment.identify(atom)
      environment_result ? environment_result : PSymbol.new(atom, environment)
    end
  end
  # This is a prototype for a parser that will be written in C, so I'm not
  # abstracting it till I'm pretty sure it's how I want it.
  # I also want to avoid anything that's going to be hard to translate.
  #
  # Sorry for the ridiculously long method.
  def self.parse(from, environment)
    to = []

    from = from.strip
    from = from[1..-2] if from =~ /^\(.*\)$/
    from = from.strip
    from = from.split('')

    i = 0
    while i < from.length
      if from[i] =~ /\s/ || from[i].empty?
        # nothing
      elsif from[i] == '\''
        if from[i+1] == '('
          # quoted lists
          i += 1
          temp = '(quote ('
          parensDepth = 1
          i += 1

          until parensDepth.zero? || i >= from.length
            temp += from[i]
            parensDepth += 1 if from[i] == '('
            parensDepth -= 1 if from[i] == ')'
            i += 1
          end

          # 'parens error' if parensDepth

          i -= 1

          to.push([self.parse(temp + ')', environment)])
        else
          # quoted atoms
          temp = ''

          i += 1
          until from[i] =~ /\s/ || from[i] == '(' || i >= from.length
            temp += from[i]
            i += 1
          end

          to.push([self.parse("(quote #{temp})", environment)])
        end
      elsif from[i] == '('
        # lists
        temp = '('
        parensDepth = 1
        i += 1

        until parensDepth.zero? || i >= from.length
          temp += from[i]
          parensDepth += 1 if from[i] == '('
          parensDepth -= 1 if from[i] == ')'
          i += 1
        end

        # 'parens error' if parensDepth

        i -= 1

        to.push(self.parse(temp, environment))
      elsif from[i] == '#' && from[i+1] == '('
        # vectors
        i += 1
        temp = '#('
        parensDepth = 1
        i += 1

        until parensDepth.zero? || i >= from.length
          temp += from[i]
          parensDepth += 1 if from[i] == '('
          parensDepth -= 1 if from[i] == ')'
          i += 1
        end

        # 'parens error' if parensDepth

        i -= 1

        to.push(PVector.new(temp))
      elsif from[i] == '"'
        # strings
        i += 1
        temp = ''
        until from[i] == '"' && from[i-1] != '\\'
          temp += from[i]
          i += 1
        end
        to.push(PString.new(temp))
      else
        # other
        temp = ''
        until from[i] =~ /\s/ || i >= from.length
          temp += from[i]
          break if from[i+1] == '('
          i += 1
        end
        to.push(self.lex(temp, environment))
      end

      i += 1 # this could be a problem
    end

    PPair.new(to, environment)
  end
end
