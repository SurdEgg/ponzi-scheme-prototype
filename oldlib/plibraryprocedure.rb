# frozen_string_literal: true

require 'lib/pproc'

# Ponzi Scheme's class for hardcoded library functions.
class PLibraryProcedure < PProc
  def initialize(name, library)
    @name = name.to_sym
    @library = library
  end 

  def to_s
    @name.to_s
  end 

  def eval(ppair, environment)
    @library.send(@name, ppair, environment)
  end
end
