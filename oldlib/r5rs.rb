require 'lib/pruleset'

class R5RS < PRuleSet
  def +(ppair, environment)
    sum = 0
    while ppair
      sum += ppair.car.eval.to_f
      ppair = ppair.cdr
    end
    PNumber.new(sum)
  end

  def -(ppair, environment)
    difference = ppair.car.eval.to_f
    ppair = ppair.cdr
    while ppair
      difference -= ppair.car.eval.to_f
      ppair = ppair.cdr
    end
    PNumber.new(difference)
  end

  def *(ppair, environment)
    product = 1
    while ppair
      product *= ppair.car.eval.to_f
      ppair = ppair.cdr
    end
    PNumber.new(product)
  end

  def /(ppair, environment)
    quotient = ppair.car.eval.to_f
    ppair = ppair.cdr
    while ppair
      quotient /= ppair.car.eval.to_f
      ppair = ppair.cdr
    end
    PNumber.new(quotient)
  end

  def define(ppair, environment)
    environment.add(ppair)
  end
end
