require 'lib/pobject'

class PNumber < PObject
  attr_reader :representation

  def initialize(given)
    @value = given.to_f
    @type = 'number'
    to_s
  end

  def to_s
    @representation = (@value % 1).zero? ? to_i.to_s : to_f.to_s
  end

  def to_i
    @value.to_i
  end

  def to_f
    @value.to_f
  end

  def eval(*args)
    self
  end
end
