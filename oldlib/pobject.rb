class PObject
  attr_reader :representation

  def initialize(given, environment)
    @environment = environment
    @representation = given.to_s
    @type = 'abstract_object'
  end

  def eval(cdr = nil, environment = nil)
    self
  end

  def to_s
    @representation
  end
end
