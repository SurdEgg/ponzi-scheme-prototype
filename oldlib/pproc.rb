# frozen_string_literal: true

require 'lib/pobject'

# Ponzi Scheme's internal proc class, representing fuctions.
class PProc < PObject
end
