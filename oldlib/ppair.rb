require 'lib/pobject'

class PPair < PObject
  attr_reader :car, :cdr

  def initialize(given, environment)
    super
    @type = 'pair'
    @car = given.shift
    @environment = environment
    if given.length.zero?
      @cdr = nil
    else
      @cdr = PPair.new(given, @environment)
    end
  end

  def to_s
    "(#{@car.to_s} #{@cdr.to_s})"
  end

  def eval(arg = nil, environment = @environment)
    @car.eval(@cdr, environment)
  end
end
