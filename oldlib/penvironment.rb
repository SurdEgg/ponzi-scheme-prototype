require 'lib/pobject'
require 'lib/r5rs'

class PEnvironment
  def initialize()
    @libraries = [R5RS.new]
    @definitions = {}
  end

  def identify(name)
    # Which should go first??
    res = nil
    @libraries.each do |l|
      if l.respond_to?(name)
        res = PLibraryProcedure.new(name, l)
        break
      end
    end
    res = @definitions[name] unless res
    res
  end

  def add(ppair)
    # The Symbol at the front of this list stops being a symbol here. That
    # seems wrong, even though I can't think why it would actually be wrong.
    # Preparet to change how this works if it turns out to be wrong.
    @definitions[ppair.car.eval.to_s] = ppair.cdr
  end
end
